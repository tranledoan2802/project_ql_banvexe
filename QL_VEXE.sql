﻿CREATE DATABASE QL_VEXE
GO
USE QL_VEXE
GO

CREATE TABLE [dbo].[Admin](
	[User] varchar(50) NOT NULL,
	[PassWord] nvarchar(50) NOT NULL
	PRIMARY KEY ([User],[PassWord])
);
GO

CREATE TABLE [dbo].[KhachHang](
	[MaKH] int NOT NULL,
	[TenKH] nvarchar(50) NULL,
	[NgaySinh] date NULL,
	[GioiTinh] nvarchar(5) NULL,
	[DiaChi] nvarchar(100) NULL,
	[SoDT]varchar(10) NULL,
	[CCCD] varchar(12) NULL,
	PRIMARY KEY ([MaKH])
);
GO
--


CREATE TABLE [dbo].[Xe](
	[MaXe] varchar(10)	NOT NULL,
	[TenXe] nvarchar(50) NULL,
	[BienSoXe] varchar(15) NULL,
	[SoGhe] int NULL
	PRIMARY KEY ([MaXe])
);
GO

CREATE TABLE [dbo].[LoaiXe](
	[MaLoaiXe] varchar(10) NOT NULL,
	[TenLoaiXe] nvarchar(50) NULL
	PRIMARY KEY ([MaLoaiXe])
);

--GO

--CREATE TABLE [dbo].[ChiTietXe](
--	[MaLoaiXe] varchar(10) NOT NULL,
--	[MaXe] varchar(10) NOT NULL,
--	[BienSoXe] varchar(15) NULL,
--	[SoGhe] int NULL
--	PRIMARY KEY ([MaLoaiXe],[MaXe])
--) ;
GO

CREATE TABLE [dbo].[VeXe](
	[MaVe] int NOT NULL,
	[MaNV] int NOT NULL,
	[MaChuyenXe] int NOT NULL,
	PRIMARY KEY ([MaVe],[MaNV],[MaChuyenXe])
);
GO
CREATE TABLE [dbo].[ChiTietVeXe](
	[HoTenKH] nvarchar(100)  NULL,
	[SoDT] varchar NOT NULL,
	[DiaChi] nvarchar(100) NULL,
	[TenTuyenXe] nvarchar(50) ,
	[TenXe] nvarchar(50),
	[DiemXuatPhat] nvarchar(50),
	[DiemDen] nvarchar(50),
	[GioXuatPhat] time,
	[GioDen] time,
	[NgayDatVe] date,
	[NgayKhoiHanh] date,
	[ThanhTien] decimal(18, 0) NULL,
	[SoLuong] int NULL,
	PRIMARY KEY ([SoDT])
);
GO


CREATE TABLE [dbo].[TuyenXe](
	[MaTuyen] varchar(10) NOT NULL,
	[TenTuyen] nvarchar(50) NULL,
	[DiemXuatPhat] nvarchar(50) NULL,
	[DiemDen] nvarchar(50) NULL,
	[BangGia] decimal(18, 0) NULL,
	[MaXe] varchar(10) NOT NULL,
	[TenXe] nvarchar(50)
	PRIMARY KEY ([MaTuyen],[MaXe])
);
GO


CREATE TABLE [dbo].[NVBanVe](
	[MaNV] int NOT NULL,
	[TenDN] varchar(50) NULL,
	[NgaySinh] date NULL,
	[GioiTinh]nvarchar(5) NULL,
	[DiaChi] nvarchar(1000) NULL,
	[CCCD] varchar(12) NULL,
	[SoDT]varchar(10) NULL,
	PRIMARY KEY ([MaNV])
);
GO

CREATE TABLE [dbo].[TaiXe](
	[MaTaiXe] int NOT NULL,
	[TenTaiXe]nvarchar(50) NULL,
	[NgaySinh] date NULL,
	[GioiTinh] nvarchar(5) NULL,
	[DiaChi] nvarchar(100) NULL,
	[CCCD] varchar(12) NULL,
	[SoDT] varchar(10) NULL,
	PRIMARY KEY ([MaTaiXe])
);

GO

CREATE TABLE [dbo].[ChuyenXe](
	[MaChuyenXe] int NOT NULL,
	[MaTuyen] varchar(10) NOT NULL,
	[GioXuatPhat] time NULL,
	[GheTrong] int NULL,
	[MaTaiXe] int NOT NULL,
	[TenTaiXe] nvarchar(50),
	PRIMARY KEY ([MaChuyenXe],[MaTuyen],[MaTaiXe] )
);
GO
--KHOA NGOẠI
--ALTER TABLE [dbo].[ChiTietXe]
--	ADD CONSTRAINT FK_CHITIETXE_LOAIXE FOREIGN KEY ([MaLoaiXe])
--	REFERENCES [dbo].[LoaiXe]([MaLoaiXe])
--ALTER TABLE [dbo].[ChiTietXe]
--	ADD CONSTRAINT FK_CHITIETXE_XE FOREIGN KEY ([MaXe])
	--REFERENCES [dbo].[Xe]([MaXe])

ALTER TABLE [dbo].[TuyenXe]
	ADD CONSTRAINT FK_TUYENXE_XE FOREIGN KEY ([MaXe])
	REFERENCES [dbo].[Xe]([MaXe])
ALTER TABLE [dbo].[VeXe]
	ADD CONSTRAINT FK_VEXE_NVBANVE FOREIGN KEY ([MaNV])
	REFERENCES [dbo].[NVBanVe]([MaNV])
--ALTER TABLE [dbo].[ChiTietVeXe]
	--ADD CONSTRAINT FK_CHITIETVEXE_VEXE FOREIGN KEY ([MaVe])
	--REFERENCES [dbo].[VeXe]([MaVe])

--RÀNG BUỘC DUY NHẤT
ALTER TABLE [Xe]
	ADD CONSTRAINT UN_CHITIETXE_BIENSOXE UNIQUE ([BienSoXe])
ALTER TABLE [KhachHang]
	ADD CONSTRAINT UN_CHITIETXE_SDT UNIQUE ([SoDT])
ALTER TABLE [KhachHang]
	ADD CONSTRAINT UN_CHITIETXE_CCCD UNIQUE ([CCCD])
ALTER TABLE [NVBanVe]
	ADD CONSTRAINT UN_NVBANVE_SDT UNIQUE ([SoDT])
ALTER TABLE [NVBanVe]
	ADD CONSTRAINT UN_NVBANVE_CCCD UNIQUE ([CCCD])
ALTER TABLE [TaiXe]
	ADD CONSTRAINT UN_TAIXE_SDT UNIQUE ([SoDT])
ALTER TABLE [TaiXe]
	ADD CONSTRAINT UN_TAIXE_CCCD UNIQUE ([CCCD])


--Lấy danh sách, thêm , xóa  và sửa
--Lấy danh sách khách hàng
go
CREATE PROC sp_LayDSKhachHang
as	select * from [dbo].[KhachHang]
exec sp_LayDSKhachHang
--Xe
go
CREATE PROC sp_LayDSXe
as	select * from [Xe]
exec sp_LayDSXe
go
CREATE PROC sp_ThemXe(@maXe varchar(10), @tenXe nvarchar(50), @bienSoXe varchar(15), @soGhe int)
as	insert into  [Xe]([MaXe],[TenXe])
values (@maXe , @tenXe)
exec sp_ThemXe 'XH001',N'Xuân Huy','77b1-1234','24'
go
CREATE PROC sp_SuaXe(@maXe varchar(10), @tenXe nvarchar(50), @bienSoXe varchar(15), @soGhe int)
as	update  [Xe]
set [TenXe]=@tenXe
where [MaXe]=@maXe
exec sp_SuaXe 'XH001',N'Xuân Huy1','77b1-1234','24'
go
CREATE PROC sp_XoaXe(@maXe varchar(10), @tenXe nvarchar(50), @bienSoXe varchar(15), @soGhe int)
as	delete from  [Xe]
where [MaXe]=@maXe
exec sp_XoaXe 'XH001',N'Xuân Huy','77b1-1234','24'
go
-- loại Xe
--CREATE PROC sp_LayDSLoaiXe
--as	select * from [LoaiXe]
--exec sp_LayDSLoaiXe

--CREATE PROC sp_ThemLoaiXe(@maLoaiXe int, @tenLoaiXe nvarchar(50))
--as	insert into  [LoaiXe]([MaLoaiXe],[TenLoaiXe])
--values (@maLoaiXe , @tenLoaiXe)
--exec sp_ThemLoaiXe '1',N'Thường'

--CREATE PROC sp_SuaLoaiXe(@maLoaiXe int, @tenLoaiXe nvarchar(50))
--as	update  [LoaiXe]
--set [TenLoaiXe]=@tenLoaiXe
--where [MaLoaiXe]=@maLoaiXe
--exec sp_SuaLoaiXe '1',N'Vip'

--CREATE PROC sp_XoaLoaiXe(@maLoaiXe int, @tenLoaiXe nvarchar(50))
--as	delete from  [LoaiXe]
--where [MaLoaiXe]=@maLoaiXe
--exec sp_XoaLoaiXe '1',N'Thường'

--Tuyến xe
CREATE PROC sp_LayDSTuyenXe
as	select * from [TuyenXe]
exec sp_LayDSTuyenXe
go
CREATE PROC sp_ThemTuyenXe(@maTuyenXe int, @tenTuyenXe nvarchar(50), @diemXuatPhat nvarchar(50),  @diemDen nvarchar(50), @bangGia decimal(18,0), @maXe varchar(10), @tenXe nvarchar(50))
as	insert into  [TuyenXe]([MaTuyen], [TenTuyen], [DiemXuatPhat], [DiemDen], [BangGia], [MaXe], [TenXe])
values (@maTuyenXe , @tenTuyenXe , @diemXuatPhat,  @diemDen, @bangGia, @maXe, @tenXe)
exec sp_ThemTuyenXe '1',N'BX_Miền Đông -> Bình định', N'BX_Miền Đông', N'Qui nhơn', '350000', 'XH001', N'Xuân Huy'
go
CREATE PROC sp_SuaTuyenXe(@maTuyenXe int, @tenTuyenXe nvarchar(50), @diemXuatPhat nvarchar(50),  @diemDen nvarchar(50), @bangGia decimal(18,0), @maXe varchar(10), @tenXe nvarchar(50))
as	update  [TuyenXe]
set [TenTuyen]=@tenTuyenXe, [DiemXuatPhat]=@diemXuatPhat, [DiemDen]=@diemDen, [BangGia]=@bangGia, [MaXe]=@maXe, [TenXe]=@tenXe
where [MaTuyen]=@maTuyenXe
exec sp_SuaTuyenXe '1',N'BX_Miền Đông -> Bình Thuận', N'BX_Miền Đông', N'Bình Thuận', '350000', 'XH001', N'Xuân Huy'
go
CREATE PROC sp_XoaTuyenXe(@maTuyenXe int, @tenTuyenXe nvarchar(50), @diemXuatPhat nvarchar(50),  @diemDen nvarchar(50), @bangGia decimal(18,0), @maXe varchar(10), @tenXe nvarchar(50))
as	delete from  [TuyenXe]
where [MaTuyen]=@maTuyenXe
exec sp_XoaTuyenXe '1',N'BX_Miền Đông -> Bình định', N'BX_Miền Đông', N'Qui nhơn', '350000', 'XH001', N'Xuân Huy'
go
--chuyến xe
CREATE PROC sp_LayDSChuyenXe
as	select * from [ChuyenXe]
exec sp_LayDSChuyenXe
go
CREATE PROC sp_ThemChuyenXe(@maChuyen int ,@maTuyenXe int, @gioXuatPhat time, @gheTrong int, @maTaiXe int)
as	insert into  [ChuyenXe]([MaChuyenXe], [MaTuyen], [GioXuatPhat], [GheTrong], [MaTaiXe])
values (@maChuyen , @maTuyenXe, @gioXuatPhat, @gheTrong, @maTaiXe)
exec sp_ThemChuyenXe '77','1','7:30','12','112'
go
CREATE PROC sp_SuaChuyenXe(@maChuyen int ,@maTuyenXe int, @gioXuatPhat time, @gheTrong int, @maTaiXe int)
as	update  [ChuyenXe]
set [MaTuyen]=@maTuyenXe, [GioXuatPhat]=@gioXuatPhat, [GheTrong]=@gheTrong, [MaTaiXe]=@maTaiXe
where [MaChuyenXe]= @maChuyen
exec sp_SuaChuyenXe '77','1','7:30','12','117'
go
CREATE PROC sp_XoaChuyenXe(@maChuyen int ,@maTuyenXe int, @gioXuatPhat time, @gheTrong int, @maTaiXe int)
as	delete from  [ChuyenXe]
where [MaChuyenXe]=@maChuyen
exec sp_XoaChuyenXe '77','1','7:30','12','112'
go
--chi tiết vé xe
CREATE PROC sp_LayDSVeXe
as	select * from [ChiTietVeXe]
exec sp_LayDSTuyenXe
go
CREATE PROC sp_ThemVeXe(@hoTenKH nvarchar(50), @soDT varchar(10), @diaChi nvarchar(100),  @tenTuyenXe nvarchar(50),@tenXe nvarchar(50) ,@diemXuatPhat nvarchar(50),  @diemDen nvarchar(50),@gioXuatPhat time,@gioDen time, @ngayDatVe date, @ngayKhoiHanh date,@soLuong int, @thanhTien decimal(18,0))
as	insert into  [ChiTietVeXe]([HoTenKH], [SoDT],[DiaChi],[TenTuyenXe],[TenXe] ,[DiemXuatPhat], [DiemDen], [GioXuatPhat], [GioDen], [NgayDatVe], [NgayKhoiHanh], [SoLuong],[ThanhTien])
values (@hoTenKH, @soDT, @diaChi, @tenTuyenXe, @tenXe, @diemXuatPhat, @diemDen, @gioXuatPhat, @gioDen, @ngayDatVe, @ngayKhoiHanh, @soLuong, @thanhTien)
exec sp_ThemVeXe N'Trần Lê Đoàn','0869738249', N'ÂN đức - Hoài ân', N'BX_Miền Đông -> Bình Định', N'Xuân Huy', N'Thái Bình', N'Hoài Ân', '18h30', '6h:30','2023/11/6', '2023/11/9','2', '700000'
go
CREATE PROC sp_DoiVeXe(@hoTenKH nvarchar(50), @soDT varchar(10), @diaChi nvarchar(100),  @tenTuyenXe nvarchar(50),@tenXe nvarchar(50) ,@diemXuatPhat nvarchar(50),  @diemDen nvarchar(50),@gioXuatPhat time,@gioDen time, @ngayDatVe date, @ngayKhoiHanh date,@soLuong int, @thanhTien decimal(18,0))
as	update  [ChiTietVeXe]
set [HoTenKH]=@hoTenKH, [DiaChi]=@diaChi, [TenTuyenXe]=@tenTuyenXe, [TenXe]=@tenXe, [DiemXuatPhat]=@diemXuatPhat, [DiemDen]=@diemDen, [GioXuatPhat]=@gioXuatPhat, [GioDen]=@gioDen, [NgayDatVe]=@ngayDatVe, [NgayKhoiHanh]= @ngayKhoiHanh, [SoLuong]= @soLuong, [ThanhTien]=@thanhTien
where [SoDT]=@soDT
exec sp_DoiVeXe N'Trần Lê Đoàn','0869738249', N'ÂN đức - Hoài ân', N'BX_Miền Đông -> Bình Định', N'Xuân Huy', N'Thái Bình', N'Hoài Ân', '18h30', '6h:30','2023/11/6', '2023/11/9','1', '350000'
go
CREATE PROC sp_HuyVeXe(@hoTenKH nvarchar(50), @soDT varchar(10), @diaChi nvarchar(100),  @tenTuyenXe nvarchar(50),@tenXe nvarchar(50) ,@diemXuatPhat nvarchar(50),  @diemDen nvarchar(50),@gioXuatPhat time,@gioDen time, @ngayDatVe date, @ngayKhoiHanh date,@soLuong int, @thanhTien decimal(18,0))
as	delete from  [ChiTietVeXe]
where [SoDT]=@soDT
exec sp_HuyVeXe  N'Trần Lê Đoàn','0869738249', N'ÂN đức - Hoài ân', N'BX_Miền Đông -> Bình Định', N'Xuân Huy', N'Thái Bình', N'Hoài Ân', '18h30', '6h:30','2023/11/6', '2023/11/9','2', '700000'
go
--Tài xế
CREATE PROC sp_LayDSTaiXe
as	select * from [TaiXe]
exec sp_LayDSTaiXe
go
CREATE PROC sp_ThemTaiXe(@maTaiXe int ,@tenTaiXe nvarchar(50), @ngaySinh date, @gioiTinh nvarchar(5), @diaChi nvarchar(100),@cCCD varchar(12), @soDT varchar(10))
as	insert into  [TaiXe]([MaTaiXe], [TenTaiXe], [NgaySinh], [GioiTinh], [DiaChi],[CCCD], [SoDT])
values (@maTaiXe , @tenTaiXe, @ngaySinh, @gioiTinh, @diaChi, @cCCD, @soDT)
exec sp_ThemTaiXe '1',N'Trần lê Đoàn','2003/02/23','Nam',N'Ân Đức','123456789101','0987654321'
go
CREATE PROC sp_SuaTaiXe(@maTaiXe int ,@tenTaiXe nvarchar(50), @ngaySinh date, @gioiTinh nvarchar(5), @diaChi nvarchar(50),@cCCD varchar(12), @soDT varchar(10))
as	update  [TaiXe]
set [TenTaiXe]=@tenTaiXe, [NgaySinh]=@ngaySinh, [GioiTinh]=@gioiTinh, [DiaChi]=@diaChi, [CCCD]= @cCCD, [SoDT]=@soDT
where [MaTaiXe]= @maTaiXe
exec sp_SuaTaiXe '1',N'Trần lê Đoàn','2003/02/23','Nam',N'Ân Đức','123456789107','0387654321'
go
CREATE PROC sp_XoaTaiXe(@maTaiXe int ,@tenTaiXe nvarchar(50), @ngaySinh date, @gioiTinh nvarchar(5), @diaChi nvarchar(50),@cCCD varchar(12), @soDT varchar(10))
as	delete from  [TaiXe]
where [MaTaiXe]=@maTaiXe
exec sp_XoaTaiXe '1',N'Trần lê Đoàn','2003/02/23','Nam',N'Ân Đức','123456789101','0987654321'
go